const plugin = require("tailwindcss/plugin");
const defaultTheme = require('tailwindcss/defaultTheme')


module.exports = {
  content: ["./src/**/*.{html,js}", "./public/*.html"],
   theme: {
    // Some useful comment
    fontFamily: {
      'sans': ['Rubik',"Creato Display",defaultTheme.fontFamily.sans],
      'serif': [...defaultTheme.fontFamily.serif],
      'mono': [...defaultTheme.fontFamily.mono]
    },
  },
  plugins: [
    plugin(function ({ addUtilities }) {
      const myUtilities = {
        ".title": {
          fontWeight: 700,
          fontSize: 40,
          fontFamily:"Creato Display",
          /* or 122% */

          color: "#293241",
        },
        ".paragraph": {
          fontFamily: "Rubik",
          fontStyle: "normal",
          fontWeight: 400,
          fontSize: 18,
          // lineHeight: 28,
          /* or 156% */

          color: "#5E5E5E",
        },
        ".phone-icone": {
          color: "#2DCA72",
        },
        ".number": {
          height: 28,
          left: "20%",
          right: "0.58%",
          top: "calc(50% - 28px/2)",

          fontFamily: "Rubik",
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: 18,
          /* identical to box height, or 156% */

          color: "#293241",
        },
        button: {
          width: 147,
          height: 47,
          background: "#2DCA72",
          borderRadius: 50,
        },
        ".buton": {
          fontFamily: "Rubik",
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: 16,
          /* identical to box height, or 106% */

          letterSpacing: 0.48,

          color: "#FFFFFF",
        },
        ".featured": {
          fontFamily: "Creato Display",
          fontStyle: "normal",
          fontWeight: 700,
          fontSize: 25,
          /* identical to box height, or 112% */

          color: "#293241",
        },
        ".certification": {
          fontFamily: "Creato Display",
          fontStyle: "normal",
          fontWeight: 700,
          fontSize: 25,

          color: "#C7C6D5",
        },
        ".line": {
          left: 42.06,
          right: "57.92",
          top: "0%",
          bottom: "0%",

          border: "1px solid #707070",
        },
        ".brand": {
          width: 230,
          height: 100,

          background: "#FFFFFF",
          boxShadow: "0px 3px 20px rgba(0, 0, 0, 0.07)",
          borderRadius: 8,
        },
        ".card1": {
          width: 287,
          height: 240,

          background: "#F2FFF8",
          borderRadius: 12,
        },
        ".typography": {
          fontFamily: "Rubik",
          fontStyle: "normal",
          fontWeight: 700,
          fontSize: 23,

          /* or 122% */

          color: "#293241",
        },
        ".card-background" : {
          width: 459,
height: 433,
// marginTop:35,

background: "#FCF1E1",
borderRadius: 83,
        },
        ".image" : {
 position:'absolute',
 width: 395,
height: 621,

marginTop:-185,


        },
        ".col2" : {
          marginTop:250

        },
        ".card-latestWork" : {
          width:394,
        },
        ".img-latestwork" : {
          borderRadius: 10,
          width: 374,
height: 250
        },
        // ----------------TESTIMONIALS--------------------------------
        ".testimonial" : {
          background: "#F2FFF8",
          height:430,

        }
,        ".card-testimonial" : {
          background: "#FFFFFF",
boxShadow: "0px 3px 30px rgba(0, 0, 0, 0.07)",
borderRadius: 12,
width:422,
height:191

        },
        ".nom-profil" : {
          top: "calc(50% - 19px/2 - 16px)",

fontFamily: 'Creato Display',
fontStyle: "normal",
fontWeight: 700,
fontSize: 16,


color: "#293241",
        },
        ".text-profil" : {
          top: "calc(50% - 28px/2 + 11.5px)",

          fontFamily: 'Rubik',
          fontStyle: "normal",
          fontWeight: 400,
          fontSize: 13,
          /* identical to box height, or 215% */
          
          
          color: "#5E5E5E",
          
        },
        ".profil-image" :{
          width:44,
          height:44,
        },
        "line-testimonial" : {
          width:30,
          border: "3px solid #000000",

        }
      };
      addUtilities(myUtilities);
    }),
  ],
};
