import React from "react";
import Section2 from "../Section2/Section2";
import Footer from "./Footer";
import Header from "./Header";
import Carte from "./Section3/Card";
import Customer from "./Section4/Customer";
import LatestWork from "./Section6/LatestWork";
import Testimonial from "./Section7/Testimonial";
import Text from "./Text";
export default function Page () {
    return(
        <div className="">
            <div className="">
                <Header/>
            </div>
            <div className="my-5">
                <Text/>
            </div>
            <div className="my-5">
                <Section2/>
            </div>
            <div className="my-15">
                <Carte/>
            </div>
            <div className="my-15">
                <Customer/>
            </div>
            <div className="my-5">
                <LatestWork/>
            </div>
               <div className="my-5">
                <Testimonial/>
            </div>
            <div className="my-5">
                <Footer/>
            </div>
        </div>
    )
}