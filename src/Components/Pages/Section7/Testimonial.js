import React from "react";
import { Card, CardBody} from "@material-tailwind/react";
import card1 from "../Pattern-fill-dc5d6cdfd54be8f5f50ace7dd35f0074.jpg"
import card2 from '../Pattern-fill-07e6496899929f0526d71a49f61f375c.jpg'
import card3 from '../Pattern-fill-cc2024ef46ca9a7d1eec2510c672e8c9.jpg'

export default  function Testimonial () {
    return(
        <div className="testimonial flex flex-col justify-center">
                  <div className="container mx-auto my-15 ">  

            <h1 className="section-title title">Testimonials</h1>
            <div
        id="main"
        className="grid grid-cols-1 justify-center   md:grid-cols-1 lg:grid-cols-3  sm:grid-cols-1    gap-1 contain-text"
      >            <Card className="">
            <CardBody className=" card-testimonial">
                <div className="flex mx-5 py-3">
                <div className=" profil-image rounded-full">
                {" "}
                <img src={card1} className="" />
              </div>
              <div className="">
                <p className="nom-profil mx-3">Zoe Manthe</p>
                <p className="text-profil mx-3">Founder, Alpha Group</p>
              </div>
                </div>
              
            
              <div className="paragraph mx-5">
              Lorem ipsum dolor sit amet, consetetur <br/>sadipscing elitr, sed diam nonumy eirmod 
              </div>
             
            </CardBody>
          </Card>
          <div className="card2-2">
          <Card className="">
            <CardBody className=" card-testimonial">
                <div className="flex mx-5 py-3">
                <div className=" profil-image rounded-full

">
                {" "}
                <img src={card2} className="" />
              </div>
              <div className="">
                <p className="nom-profil mx-3">Don Joe</p>
                <p className="text-profil mx-3">Founder, Alpha Group</p>
              </div>
                </div>
              
            
              <div className="paragraph mx-5">
              Lorem ipsum dolor sit amet, consetetur <br/>sadipscing elitr, sed diam nonumy eirmod 
              </div>
             
            </CardBody>
          </Card>
          </div>
          <div className="card3-3">
          <Card className="">
            <CardBody className=" card-testimonial">
                <div className="flex mx-5 py-3">
                <div className=" profil-image rounded-full

">
                {" "}
                <img src={card3} className="" />
              </div>
              <div className="">
                <p className="nom-profil mx-3">Natalie Colin</p>
                <p className="text-profil mx-3">Founder, Alpha Group</p>
              </div>
                </div>
              
            
              <div className="paragraph mx-5">
              Lorem ipsum dolor sit amet, consetetur <br/>sadipscing elitr, sed diam nonumy eirmod 
              </div>
             
            </CardBody>
          </Card>
          </div>

          <div className="line-testimonial mx-3"></div>

            </div>
            <div className="">
                {/* <div className="line-testimonial mx-3"></div>
                <div className="line-testimonial mx-3"></div> */}


            </div>
            </div>

        </div>
    )
 }