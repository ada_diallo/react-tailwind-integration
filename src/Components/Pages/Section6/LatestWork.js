import React from "react";
import { Card, CardBody} from "@material-tailwind/react";
import card1 from "../Pattern-fill-e6b5de8e170a2e8acaec98979d01feb6.jpg"
import card2 from '../Pattern-fill-a5f7b9f695af0c074872e10a39492de1.jpg'
import card3 from '../Pattern-fill-e37c23a0f5211e59dd2a24560c4838a3.jpg'

export default  function LatestWork () {
    return(
        <div className="container mx-auto ">
            <h1 className="section-title title">Our latest work</h1>
            <div
        id="main"
        className="grid grid-cols-1 justify-center   md:grid-cols-1 lg:grid-cols-3  sm:grid-cols-1    gap-1 contain-text"
      >            <Card className="card-latestWork ">
            <CardBody className=" ">
              <div className="img-latestwork">
                {" "}
                <img src={card1} className="" />
              </div>
              <div  className="typography">
              Artem - Digital Marketing <br/>
              campaign
              </div>
              <div className="paragraph">
              Lorem ipsum dolor sit amet, consetetur <br/>sadipscing elitr, sed diam nonumy eirmod 
              </div>
             
            </CardBody>
          </Card>
          <div className="card2-2">
          <Card className="card-latestWork ">
            <CardBody className=" ">
              <div className="">
                {" "}
                <img src={card2} className="" />
              </div>
              <div  className="typography">
              Mayhem - Search engine
              <br/>Optimization
              </div>
              <div className="paragraph">
              Lorem ipsum dolor sit amet, consetetur <br/>sadipscing elitr, sed diam nonumy eirmod 
              </div>
             
            </CardBody>
          </Card>
          </div>
          <div className="card3-3">
          <Card className="card-latestWork ">
            <CardBody className=" ">
              <div className="">
                {" "}
                <img src={card3} className="" />
              </div>
              <div  className="typography">
              Basic - Pay per click<br/>
              (PPC)
              </div>
              <div className="paragraph">
              Lorem ipsum dolor sit amet, consetetur <br/>sadipscing elitr, sed diam nonumy eirmod 
              </div>
             
            </CardBody>
          </Card>
          </div>


            </div>

        </div>
    )
 }