import React from "react";
import Button from "./Button";
import footerImage from "./Pattern-fill-12d68db06394b05f4f1fafb8f501b18e.jpg";
import { BsTelephoneFill } from "react-icons/bs";

export default function Footer() {
  return (
    <div className="container mx-auto my-12 ">
      <div
        id="main"
        className="grid grid-cols-1 justify-center   md:grid-cols-2 lg:grid-cols-2  sm:grid-cols-1    gap-1 contain-text my-12"
      >
        <div class="py-20">
          <h1 className="title">
            Creative Digital <br />
            Marketing Agency
          </h1>
          <p className="mt-8 paragraph ">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </p>
          <div className="flex ">
            <div className="">
              <Button />
            </div>
            <div className="flex items-center mx-10"> Or</div>
            <div class="flex items-center">
              <div className="phone-icone py-1">
                {" "}
                <BsTelephoneFill size={18} />
              </div>
              <p className="mx-1 number"> +0 123 456-789</p>
            </div>
          </div>
        </div>
        <div className="flex justify-end">
          <img src={footerImage} className="" />
        </div>
      </div>
    </div>
  );
}
