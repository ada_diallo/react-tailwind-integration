import React from "react";
import { BsTelephoneFill } from "react-icons/bs";
import logo from './Logo.jpg'

export default function Header() {
  return (
    <div className="container mx-auto">
      <div className="flex  flex-row justify-between my-10 ">
<img src={logo}/>

        <div class="flex ">
          <div className="phone-icone py-1">
            {" "}
            <BsTelephoneFill size={18} />
          </div>
          <p className="mx-1 number"> +0 123 456-789</p>
        </div>
      </div>

   
    </div>
  );
}
