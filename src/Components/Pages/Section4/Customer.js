import React from "react";
import group from "../Group 484.jpg";
import mainImage from "../Pattern-fill-189f3959b02b08c8fe30026dfcdf3e1e.png";
import { Card, CardBody, Typography } from "@material-tailwind/react";
import Button from "../Button";

export default function Customer() {
  return (
    <div className="container mx-auto my-20 sm:flex ">
  <div
        id="main"
        className="grid grid-cols-1 justify-center   md:grid-cols-2 lg:grid-cols-2  sm:grid-cols-1    gap-1 contain-text my-12"
      >        <div className="flex">
          <Card className="card-background flex justify-center items-center my-20">
            <CardBody className="  image  ">
              <div className="">
                {" "}
                <img src={mainImage} className="" />
              </div>
             
            </CardBody>
          </Card>
          <div className=" brand my-0 ">
            <div className="mx-5 my-3">
            <p className=" paragraph">Our Customers</p>
                <img src={group} />
            </div>
               
              </div>
        </div>
        <div class="my-20">
          <h1 className="title">
            Why should you <br />
            choose Ozone
          </h1>
          <p className="mt-8 paragraph ">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor Lorem ipsum dolor sit amet consetetur
            sadipscing elitr
          </p>
          <div className="">
            <Button />
          </div>
        </div>{" "}
      </div>
    </div>
  );
}
