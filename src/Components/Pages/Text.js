import React from "react";
import Button from "./Button";
import profil from "./Pattern-fill-189f3959b02b08c8fe30026dfcdf3e1e.png"

export default function Text () {
    return(
        <div className="container mx-auto my-12    ">
  <div
        id="main"
        className="grid grid-cols-1 justify-center   md:grid-cols-2 lg:grid-cols-2  sm:grid-cols-1    gap-1 contain-text"
      >        <div class="py-20">
          <h1 className="title">
            Creative Digital <br />
            
            Marketing Agency
          </h1>
          <p className="mt-8 paragraph ">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. 
          </p>
          <div className="">
        <Button/>
      </div>
        </div>
        <div className="flex justify-end md:w-98">
            <img src={profil} className=""/>
        </div>
      </div>
     
        </div>
    )
}