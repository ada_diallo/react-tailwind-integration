import React from "react";
import BrandLogo from "./BrandLogo";
import Header2 from "./Header2";
export default  function Section2 () {
    return(
        <div className="container mx-auto">
            <div className="">
                <Header2/>
            </div>
            <div className="my-8">
                <BrandLogo/>
            </div>
        </div>
    )
}