import React from "react";
import asusLogo from "../Pages/asus-logo.jpg"
import path from "../Pages/Path 2342.png"
import path2 from "../Pages/chase.jpg"
import path3 from "../Pages/new-york-times.jpg"
import path4 from "../Pages/linkedin.jpg"

export default function BrandLogo() {
  return (
    <div className="container mx-auto my-12">
<div
        id="main"
        className="grid grid-cols-1 justify-center   md:grid-cols-5 md:mx-5 lg:grid-cols-5   sm:grid-cols-5    gap-4 contain-text"
      >        <div className="brand flex items-center justify-center ">
                   <img src={asusLogo} className=""/>

        </div>
        <div className="brand flex items-center justify-center ">
            <img src={path}/>
        </div>
        <div className="brand flex items-center justify-center ">
            <img src={path2}/>
        </div>
        <div className="brand flex items-center justify-center ">
            <img src={path3}/>
        </div>
        <div className="brand flex items-center justify-center ">
            <img src={path4}/>
        </div>




      </div>
    </div>
  );
}
