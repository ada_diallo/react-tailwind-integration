import { BrowserRouter, Routes, Route } from "react-router-dom";
import Button from "./Components/Pages/Button";
import Footer from "./Components/Pages/Footer";
import Header from "./Components/Pages/Header";
import Page from "./Components/Pages/Page";
import Carte from "./Components/Pages/Section3/Card";
import Customer from "./Components/Pages/Section4/Customer";
import LatestWork from "./Components/Pages/Section6/LatestWork";
import Testimonial from "./Components/Pages/Section7/Testimonial";
import Text from "./Components/Pages/Text";
import BrandLogo from "./Components/Section2/BrandLogo";
import Header2 from "./Components/Section2/Header2";
import Section2 from "./Components/Section2/Section2";
// import './Components/Styles/index.pcss'
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/header" element={<Header />} />
          <Route path="/body" element={<Text/>} />

          <Route path="/buton" element={<Button />} />
          <Route path="/" element={<Page />} />
          <Route path="/header2" element={<Header2 />} />
          <Route path="/brandlogo" element={<BrandLogo />} />
          <Route path="/section2" element={<Section2 />} />
          <Route path="/card" element={<Carte />} />
          <Route path="/customer" element={<Customer/>} />
          <Route path="/latestwork" element={<LatestWork/>} />
          <Route path="/testimonial" element={<Testimonial/>} />
          <Route path="/footer" element={<Footer/>} />









         
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
